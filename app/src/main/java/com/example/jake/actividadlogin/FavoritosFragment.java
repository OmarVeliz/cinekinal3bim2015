package com.example.jake.actividadlogin;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.actividadlogin.SqlLite.DbHelper;
import com.example.jake.actividadlogin.beans.Favoritos;
import com.example.jake.actividadlogin.beans.Peliculas;
import com.example.jake.actividadlogin.manejador.ManejadorFavorito;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static com.example.jake.actividadlogin.manejador.ManejadorFavorito.*;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends Fragment {

    private ListView favoritos;

    public FavoritosFragment() {
        // Required empty public constructor
    }

    //String strings[] = new String[100];

    //ArrayList<Favoritos> agregarFavorito = new ArrayList<Favoritos>();
    private AdaptadorFavoritos adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_favoritos, container, false);

        ManejadorFavorito.getInstancia().limpiarFavorito();
        favoritos = (ListView)v.findViewById(R.id.mostrarFavoritos);

        //Favoritos

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        String nameUsuario = pref.getString("nombre", null);

        //Db

        DbHelper favoritosDatabase = new DbHelper(getActivity().getApplicationContext(), "DBCine", null, 1);
        SQLiteDatabase db = favoritosDatabase.getWritableDatabase();

        //Llenar el listview favoritos

        Favoritos ver;
        Cursor cursor= db.rawQuery("SELECT titleMovie, generoMovie FROM Favoritos WHERE nameUser = '" + nameUsuario + "'", null);
        if (cursor.moveToFirst()){
            do {
                //Favoritos favoritos1 = new Favoritos(cursor.getInt(0), cursor.getString(1), cursor.getInt(2));
                ver = new Favoritos();
                ver.setTitleMovie(cursor.getString(0));
                ver.setGeneroMovie(cursor.getString(1));
                ManejadorFavorito.getInstancia().nuevoFavorito(ver);
            }while (cursor.moveToNext());
        }

        db.close();
        adaptador = new AdaptadorFavoritos(getActivity().getApplicationContext(), ManejadorFavorito.getInstancia().agregarFavorito);
        favoritos.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
        registerForContextMenu(favoritos);
        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();

        if(v.getId() == R.id.mostrarFavoritos){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Favoritos favorito = (Favoritos) favoritos.getAdapter().getItem(info.position);
            menu.setHeaderTitle(favorito.getTitleMovie());
            inflater.inflate(R.menu.ctx_eliminar_favorito, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Favoritos nombrePelicula = (Favoritos) favoritos.getAdapter().getItem(info.position);

        switch (item.getItemId()) {

            case R.id.CtxListEliminarFavorito:
                try {

                    String guardarTitulo = nombrePelicula.getTitleMovie();

                    DbHelper movies = new DbHelper(getActivity().getApplicationContext(), "DBCine", null, 1);
                    SQLiteDatabase db = movies.getWritableDatabase();

                    db.execSQL("DELETE FROM Favoritos WHERE titleMovie = '" + guardarTitulo + "'");

                    //Recargar nota

                    Fragment fragment = null;
                    fragment = new FavoritosFragment();

                    getFragmentManager().beginTransaction()
                            .replace(R.id.ContainerLayout, fragment)
                            .commit();

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No visible", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    class AdaptadorFavoritos extends ArrayAdapter<Favoritos> {
        public AdaptadorFavoritos(Context context, ArrayList<Favoritos> datos) {
            super(context, R.layout.listener_favorito, datos);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View item = inflater.inflate(R.layout.listener_favorito, null);

            TextView nombreP = (TextView) item.findViewById(R.id.vistaFavorito);
            TextView generoP = (TextView) item.findViewById(R.id.generoFavorito);
            ImageView imagenP = (ImageView)item.findViewById(R.id.imageFavorite);

            String nombre = ManejadorFavorito.getInstancia().agregarFavorito.get(position).getTitleMovie();
            String genero = ManejadorFavorito.getInstancia().agregarFavorito.get(position).getGeneroMovie();

            imagenP.setImageResource(R.drawable.ic_favorite);
            nombreP.setText(nombre);
            generoP.setText(genero);
            return item;
        }
    }


}
