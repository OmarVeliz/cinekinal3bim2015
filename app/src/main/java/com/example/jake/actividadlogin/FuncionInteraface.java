package com.example.jake.actividadlogin;

import com.example.jake.actividadlogin.beans.Functions;

import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by jake on 19/07/15.
 */
public interface FuncionInteraface {

    @GET("/mostrarfunciones")
    void getFunctions(Callback<List<Functions>> callback);
}
