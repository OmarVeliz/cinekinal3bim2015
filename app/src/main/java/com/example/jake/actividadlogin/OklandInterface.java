package com.example.jake.actividadlogin;

import com.example.jake.actividadlogin.beans.Functions;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by jake on 24/07/15.
 */
public interface OklandInterface {
    @GET("/mostrarfunciones/1")
    void getOkland(Callback<List<Functions>> callback);
}
