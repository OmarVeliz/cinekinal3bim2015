package com.example.jake.actividadlogin;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jake.actividadlogin.SqlLite.DbHelper;
import com.example.jake.actividadlogin.beans.Usuario;
import com.example.jake.actividadlogin.manejador.ManejadorUsuario;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class Register extends ActionBarActivity {

    EditText newUser;
    EditText newPass;
    EditText repetPass;
    EditText nombreYApell;
    EditText correo;
    private Toolbar mToolbar;
    private Button btnRegistro;
    String nombreUsuario, contraseniaUsuario, datosUsuario, correoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!isOnline()){
            Toast.makeText(Register.this.getApplicationContext(), "Please chech your network status", Toast.LENGTH_LONG).show();
        }

        //Parse.com
        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        newUser = (EditText)findViewById(R.id.textNuevoUsuario);
        newPass = (EditText)findViewById(R.id.textNuevaContrasenia);
        repetPass = (EditText)findViewById(R.id.textRepetirNuevaContrasenia);
        nombreYApell = (EditText)findViewById(R.id.EditTextNombreYapellido);
        correo = (EditText)findViewById(R.id.EditTextCorreo);

        btnRegistro = (Button)findViewById(R.id.btnNuevoRegistro);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String verificarPass = newPass.getText().toString();
                String verificarRepPass = repetPass.getText().toString();
                correoUsuario = correo.getText().toString();

                if (correo.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+") && correo.length() > 0)
                {
                    if(verificarPass.equals(verificarRepPass)) {
                        try {
                            //Parse
                            nombreUsuario = newUser.getText().toString();
                            contraseniaUsuario = newPass.getText().toString();
                            datosUsuario = nombreYApell.getText().toString();
                            correoUsuario = correo.getText().toString();

                            ParseUser usuario = new ParseUser();
                            usuario.setUsername(nombreUsuario);
                            usuario.setPassword(contraseniaUsuario);
                            usuario.put("Nombre", datosUsuario);
                            usuario.setEmail(correoUsuario);

                            usuario.signUpInBackground(new SignUpCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        Toast.makeText(getApplicationContext(), "Se ha completado con exito",
                                                Toast.LENGTH_LONG).show();

                                        Intent pestañaLogin = new Intent(Register.this, Login.class);
                                        startActivity(pestañaLogin);
                                        Register.this.finish();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Registro fallido",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "No se ha podido realizar",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Contrasenia invalida",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Email invalido",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean isOnline(){
        ConnectivityManager manager = (ConnectivityManager)this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
