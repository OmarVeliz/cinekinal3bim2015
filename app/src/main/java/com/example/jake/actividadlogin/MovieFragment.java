package com.example.jake.actividadlogin;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.actividadlogin.SqlLite.DbHelper;
import com.example.jake.actividadlogin.beans.Movie;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

//Cargar imagenes
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {


    //Mostrar datos

    private ListView peliculas;

    public MovieFragment() {
        // Required empty public constructor
    }

    private String nameUsuario;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_movie, container, false);

        //ListView

        peliculas = (ListView)v.findViewById(R.id.peliculas);

        if (!isOnline()){
            Toast.makeText(getActivity().getApplicationContext(), "Please chech your network status", Toast.LENGTH_LONG).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.14/cinekinal/public/api").build();

        PeliculasInterface service = restAdapter.create(PeliculasInterface.class);

        service.getMovie(new Callback<List<Movie>>() {
            @Override
            public void success(List<Movie> movies, Response response) {
                AdaptadorPelicula adaptadorPelicula = new AdaptadorPelicula(getActivity().getApplicationContext(), movies);
                peliculas.setAdapter(adaptadorPelicula);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        peliculas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentTitular = new Intent(getActivity().getApplicationContext(), mostrarDetalles.class);
                Movie peliculaSeleccionada = (Movie) parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", peliculaSeleccionada.getTitulo());
                extras.putString("Descripcion", peliculaSeleccionada.getSinopsis());
                extras.putString("Genero", peliculaSeleccionada.getGenero());
                extras.putString("Clasificacion", peliculaSeleccionada.getRated());
                extras.putString("Trailer", peliculaSeleccionada.getTrailer_url());
                extras.putString("Imagen", peliculaSeleccionada.getImage());
                intentTitular.putExtras(extras);
                startActivity(intentTitular);
            }
        });


        registerForContextMenu(peliculas);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity().getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        return v;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();

        if(v.getId() == R.id.peliculas){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Movie titularMenu = (Movie) peliculas.getAdapter().getItem(info.position);
            menu.setHeaderTitle(titularMenu.getTitulo());
            inflater.inflate(R.menu.menu_ctx_pelicula, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Movie titularMenu = (Movie) peliculas.getAdapter().getItem(info.position);
        //Favoritos agregarFavorito = (Favoritos) favorito.getAdapter().getItem(info.position);

        switch (item.getItemId()){
            case R.id.CtxListPeliculaFavorito:
                try {
                    SharedPreferences pref = getActivity().getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                    nameUsuario = pref.getString("nombre", null);

                    DbHelper favorit = new DbHelper(getActivity(), "DBCine", null, 1);
                    SQLiteDatabase db = favorit.getWritableDatabase();
                    db.execSQL("INSERT INTO Favoritos VALUES (null, '" + titularMenu.getTitulo() + "', '" +  titularMenu.getGenero() + "', '" + nameUsuario + "')");
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "No se ha podido agregar", Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.CtxListDetalle:
                Intent pestañaDetalles = new Intent(getActivity().getApplicationContext(), mostrarDetalles.class);
                Bundle extras = new Bundle();
                extras.putString("Titulo", titularMenu.getTitulo());
                extras.putString("Descripcion", titularMenu.getSinopsis());
                extras.putString("Genero", titularMenu.getGenero());
                extras.putString("Clasificacion", titularMenu.getRated());
                extras.putString("Trailer", titularMenu.getTrailer_url());
                extras.putString("Imagen", titularMenu.getImage());
                pestañaDetalles.putExtras(extras);
                startActivity(pestañaDetalles);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isOnline(){
        ConnectivityManager manager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        } else {
            return false;
        }
    }


    class AdaptadorPelicula extends ArrayAdapter<Movie> {

        private List<Movie> listaPeliculas;

        public AdaptadorPelicula(Context context, List<Movie> movie) {
            super(context, R.layout.list_pelicula, movie);
            listaPeliculas = movie;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.list_pelicula, null);

            ImageLoader imageLoader = ImageLoader.getInstance();

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_movie)
                    .showImageOnFail(R.drawable.ic_movie)
                    .showImageOnLoading(R.drawable.ic_movie).build();

                ImageView imageView = (ImageView)item.findViewById(R.id.imageFavorite);

                imageLoader.displayImage(listaPeliculas.get(position).getImage(), imageView, options);

                TextView textTitulo = (TextView)item.findViewById(R.id.FavoritoTitulo);
                textTitulo.setText("Titulo: " + listaPeliculas.get(position).getTitulo());

                TextView textSinopsis = (TextView)item.findViewById(R.id.GeneroPelicula);
                textSinopsis.setText("Genero: " + listaPeliculas.get(position).getGenero());

                TextView textClasificacion = (TextView)item.findViewById(R.id.ClasificacionPelicula);
                textClasificacion.setText("Clasificacion: " + listaPeliculas.get(position).getRated());


            return item;
        }
    }
}

