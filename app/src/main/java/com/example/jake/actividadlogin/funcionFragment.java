package com.example.jake.actividadlogin;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.actividadlogin.beans.Functions;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class funcionFragment extends Fragment {


    private ListView funciones;
    private ListView funcionesOkland;
    private ListView funcionesMiraflores;
    private ListView funcionesCayala;

    private List<Functions> listaFunciones;
    private List<Functions> listaOkland;
    private List<Functions> listaMiraflores;
    private List<Functions> listaCayala;

    public funcionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View v = inflater.inflate(R.layout.fragment_funcion, container, false);

        Resources res = getResources();
        TabHost tabs = (TabHost)v.findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec("miTab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("General", res.getDrawable(android.R.drawable.ic_menu_recent_history));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("miTab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Okland", res.getDrawable(android.R.drawable.btn_star));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("miTab3");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Miraflores", res.getDrawable(android.R.drawable.btn_star));
        tabs.addTab(spec);

        spec = tabs.newTabSpec("miTab3");
        spec.setContent(R.id.tab4);
        spec.setIndicator("Cayala", res.getDrawable(android.R.drawable.btn_star));
        tabs.addTab(spec);

        funciones = (ListView)v.findViewById(R.id.funciones);
        funcionesOkland = (ListView)v.findViewById(R.id.funcionesOkland);
        funcionesMiraflores = (ListView)v.findViewById(R.id.funcionesMiraflores);
        funcionesCayala = (ListView)v.findViewById(R.id.funcionesCayala);

        if (!isOnline()){
            Toast.makeText(getActivity().getApplicationContext(), "Please chech your network status", Toast.LENGTH_LONG).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.14/cinekinal/public/api").build();

        FuncionInteraface service = restAdapter.create(FuncionInteraface.class);
        OklandInterface serviceOkland = restAdapter.create(OklandInterface.class);
        MirafloresInterface serviceMiraflores = restAdapter.create(MirafloresInterface.class);
        CayalaInterface serviceCayala = restAdapter.create(CayalaInterface.class);

        service.getFunctions(new Callback<List<Functions>>() {
            @Override
            public void success(List<Functions> funcionesM, Response response) {
                AdaptadorFunciones adaptadorFunciones = new AdaptadorFunciones(getActivity().getApplicationContext(), funcionesM);
                funciones.setAdapter(adaptadorFunciones);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        serviceOkland.getOkland(new Callback<List<Functions>>() {
            @Override
            public void success(List<Functions> funcionesL, Response response) {
                AdaptadorOkland adaptadorOkland = new AdaptadorOkland(getActivity().getApplicationContext(), funcionesL);
                funcionesOkland.setAdapter(adaptadorOkland);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        serviceMiraflores.getMiraflores(new Callback<List<Functions>>() {
            @Override
            public void success(List<Functions> funcionesY, Response response) {
                AdaptadorMiraflores adaptadorMiraflores = new AdaptadorMiraflores(getActivity().getApplicationContext(), funcionesY);
                funcionesMiraflores.setAdapter(adaptadorMiraflores);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        serviceCayala.getCayala(new Callback<List<Functions>>() {
            @Override
            public void success(List<Functions> funcionesH, Response response) {
                AdaptadorCayala adaptadorCayala = new AdaptadorCayala(getActivity().getApplicationContext(), funcionesH);
                funcionesCayala.setAdapter(adaptadorCayala);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity().getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        return v;
    }


    public boolean isOnline(){
        ConnectivityManager manager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        } else {
            return false;
        }
    }

    class AdaptadorFunciones extends ArrayAdapter<Functions> {

        public AdaptadorFunciones(Context context, List<Functions> funciones) {
            super(context, R.layout.list_funciones, funciones);
            listaFunciones = funciones;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.list_funciones, null);

            ImageLoader imageLoader = ImageLoader.getInstance();

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_movie)
                    .showImageOnFail(R.drawable.ic_movie)
                    .showImageOnLoading(R.drawable.ic_movie).build();

            ImageView imageFunciones = (ImageView)item.findViewById(R.id.imageFunciones);

            imageLoader.displayImage(listaFunciones.get(position).getImage(), imageFunciones, options);

            TextView textTituloP = (TextView)item.findViewById(R.id.tituloMovie);
            textTituloP.setText("Titulo: " + listaFunciones.get(position).getTitulo());

            TextView textSala = (TextView)item.findViewById(R.id.tipoSala);
            textSala.setText("Sala: " + listaFunciones.get(position).getNombre());

            TextView fechaMovie = (TextView)item.findViewById(R.id.fechaMovie);
            fechaMovie.setText("Fecha: " + listaFunciones.get(position).getFecha());

            TextView hourMovie = (TextView)item.findViewById(R.id.hour);
            hourMovie.setText("Hora: " + listaFunciones.get(position).getHora());

            TextView formatMovie = (TextView)item.findViewById(R.id.formatLenguaje);
            formatMovie.setText("Formato: " + listaFunciones.get(position).getFormato_lenguaje());

            return item;
        }
    }

    class AdaptadorOkland extends ArrayAdapter<Functions> {

        public AdaptadorOkland(Context context, List<Functions> funcionesO) {
            super(context, R.layout.list_okland, funcionesO);
            listaOkland = funcionesO;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.list_okland, null);

            ImageLoader imageLoader = ImageLoader.getInstance();

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_movie)
                    .showImageOnFail(R.drawable.ic_movie)
                    .showImageOnLoading(R.drawable.ic_movie).build();

            ImageView imageOkland = (ImageView)item.findViewById(R.id.imageOkland);

            imageLoader.displayImage(listaOkland.get(position).getImage(), imageOkland, options);

            TextView textTituloP = (TextView)item.findViewById(R.id.tituloMovieK);
            textTituloP.setText("Titulo: " + listaOkland.get(position).getTitulo());

            TextView textSala = (TextView)item.findViewById(R.id.tipoSalaK);
            textSala.setText("Sala: " + listaOkland.get(position).getNombre());

            TextView fechaMovie = (TextView)item.findViewById(R.id.fechaMovieK);
            fechaMovie.setText("Fecha: " + listaOkland.get(position).getFecha());

            TextView hourMovie = (TextView)item.findViewById(R.id.hourK);
            hourMovie.setText("Hora: " + listaOkland.get(position).getHora());

            TextView formatMovie = (TextView)item.findViewById(R.id.formatLenguajeK);
            formatMovie.setText("Formato: " + listaOkland.get(position).getFormato_lenguaje());

            return item;
        }
    }

    class AdaptadorMiraflores extends ArrayAdapter<Functions> {

        public AdaptadorMiraflores(Context context, List<Functions> funcionesM) {
            super(context, R.layout.list_miraflores, funcionesM);
            listaMiraflores = funcionesM;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.list_miraflores, null);

            ImageLoader imageLoader = ImageLoader.getInstance();

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_movie)
                    .showImageOnFail(R.drawable.ic_movie)
                    .showImageOnLoading(R.drawable.ic_movie).build();

            ImageView imageMiraflores = (ImageView)item.findViewById(R.id.imageMiraflores);

            imageLoader.displayImage(listaMiraflores.get(position).getImage(), imageMiraflores, options);

            TextView textTituloP = (TextView)item.findViewById(R.id.tituloMovieM);
            textTituloP.setText("Titulo: " + listaMiraflores.get(position).getTitulo());

            TextView textSala = (TextView)item.findViewById(R.id.tipoSalaM);
            textSala.setText("Sala: " + listaMiraflores.get(position).getNombre());

            TextView fechaMovie = (TextView)item.findViewById(R.id.fechaMovieM);
            fechaMovie.setText("Fecha: " + listaMiraflores.get(position).getFecha());

            TextView hourMovie = (TextView)item.findViewById(R.id.hourM);
            hourMovie.setText("Hora: " + listaMiraflores.get(position).getHora());

            TextView formatMovie = (TextView)item.findViewById(R.id.formatLenguajeM);
            formatMovie.setText("Formato: " + listaMiraflores.get(position).getFormato_lenguaje());

            return item;
        }
    }

    class AdaptadorCayala extends ArrayAdapter<Functions> {

        public AdaptadorCayala(Context context, List<Functions> funcionesC) {
            super(context, R.layout.list_cayala, funcionesC);
            listaCayala = funcionesC;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.list_cayala, null);

            ImageLoader imageLoader = ImageLoader.getInstance();

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_movie)
                    .showImageOnFail(R.drawable.ic_movie)
                    .showImageOnLoading(R.drawable.ic_movie).build();

            ImageView imageCayala = (ImageView)item.findViewById(R.id.imageCayala);

            imageLoader.displayImage(listaCayala.get(position).getImage(), imageCayala, options);

            TextView textTituloP = (TextView)item.findViewById(R.id.tituloMovieC);
            textTituloP.setText("Titulo: " + listaCayala.get(position).getTitulo());

            TextView textSala = (TextView)item.findViewById(R.id.tipoSalaC);
            textSala.setText("Sala: " + listaCayala.get(position).getNombre());

            TextView fechaMovie = (TextView)item.findViewById(R.id.fechaMovieC);
            fechaMovie.setText("Fecha: " + listaCayala.get(position).getFecha());

            TextView hourMovie = (TextView)item.findViewById(R.id.hourC);
            hourMovie.setText("Hora: " + listaCayala.get(position).getHora());

            TextView formatMovie = (TextView)item.findViewById(R.id.formatLenguajeC);
            formatMovie.setText("Formato: " + listaCayala.get(position).getFormato_lenguaje());

            return item;
        }
    }


}