package com.example.jake.actividadlogin;


import com.example.jake.actividadlogin.beans.PreEstrenos;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by jake on 19/07/15.
 */
public interface PreEstrenoInterface {

    @GET("/estrenos")
    void getPreEstrenos(Callback<List<PreEstrenos>> callback);
}
