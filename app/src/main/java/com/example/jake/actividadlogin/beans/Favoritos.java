package com.example.jake.actividadlogin.beans;

/**
 * Created by Jake on 11/06/2015.
 */
public class Favoritos {
    private int id;
    private String titleMovie = null;
    private String generoMovie = null;
    private int idUsuario;

   // public void setId(int id) {
      //  this.id = id;
    //}

    public void setTitleMovie(String titleMovie) {
        this.titleMovie = titleMovie;
    }

    //public void setIdUsuario(int idUsuario) {
      // this.idUsuario = idUsuario;
    //}

    //public int getId() {
        //return id;
    //}

    public String getTitleMovie() {
        return titleMovie;
    }

    //public int getIdUsuario() {
      //  return idUsuario;
    //}


    public String getGeneroMovie() {
        return generoMovie;
    }

    public void setGeneroMovie(String generoMovie) {
        this.generoMovie = generoMovie;
    }
}
