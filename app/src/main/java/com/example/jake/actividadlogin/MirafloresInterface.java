package com.example.jake.actividadlogin;

import com.example.jake.actividadlogin.beans.Functions;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by jake on 24/07/15.
 */
public interface MirafloresInterface {

    @GET("/mostrarfunciones/2")
    void getMiraflores(Callback<List<Functions>> callback);

}
