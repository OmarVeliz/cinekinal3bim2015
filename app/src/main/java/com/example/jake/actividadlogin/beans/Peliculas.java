package com.example.jake.actividadlogin.beans;

/**
 * Created by Jake on 22/05/2015.
 */
public class Peliculas {

    private String nombrePelicula;
    private String descripcionPelicula;

    public String getNombrePelicula() {
        return nombrePelicula;
    }

    public String getDescripcionPelicula() {
        return descripcionPelicula;
    }

    public void setNombrePelicula(String nombrePelicula) {
        this.nombrePelicula = nombrePelicula;
    }

    public void setDescripcionPelicula(String descripcionPelicula) {
        this.descripcionPelicula = descripcionPelicula;
    }

    public Peliculas(String nombrePelicula, String descripcionPelicula) {
        this.nombrePelicula = nombrePelicula;
        this.descripcionPelicula = descripcionPelicula;
    }
}
