package com.example.jake.actividadlogin.beans;

/**
 * Created by Jake on 21/05/2015.
 */
public class Usuario {
    public int id;
    public  String nombreUsuario;
    public String contraseniaUsuario;
    public String nombreYapellido;
    public String correoUsuario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreUsuario(){
        return nombreUsuario;
    }

    public String getContraseniaUsuario() {
        return  contraseniaUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public void setContraseniaUsuario(String contraseniaUsuario) {
        this.contraseniaUsuario = contraseniaUsuario;
    }

    public void setNombreYapellido(String nombreYapellido) {
        this.nombreYapellido = nombreYapellido;
    }

    public String getNombreYapellido() {
        return nombreYapellido;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public Usuario(int id, String nombreUsuario, String contraseniaUsuario, String nombreYapellido, String correoUsuario) {
        this.id = id;
        this.nombreUsuario = nombreUsuario;
        this.contraseniaUsuario = contraseniaUsuario;
        this.nombreYapellido = nombreYapellido;
        this.correoUsuario = correoUsuario;
    }
}
