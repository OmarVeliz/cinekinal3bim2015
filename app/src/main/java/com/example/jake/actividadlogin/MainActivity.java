package com.example.jake.actividadlogin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.actividadlogin.beans.PreEstrenos;
import com.example.jake.actividadlogin.manejador.ManejadorFavorito;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends ActionBarActivity implements NavigationDrawerFragment.FragmentDrawerListener {

    //Usuario

    private String nameUser;
    private String correUser;
    private ListView estrenos;

    private ListView lvItems;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toolbar

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Nav Drawner

        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragmnet);
        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, R.id.navigation_drawer_fragmnet);
        drawerFragment.setDrawerListener(this);

        //Estrenos
        estrenos = (ListView) findViewById(R.id.estrenos);

        if (!isOnline()){
            Toast.makeText(this, "Please chech your network status", Toast.LENGTH_LONG).show();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.14/cinekinal/public/api").build();

        PreEstrenoInterface service = restAdapter.create(PreEstrenoInterface.class);

        service.getPreEstrenos(new Callback<List<PreEstrenos>>() {
            @Override
            public void success(List<PreEstrenos> PreEstr, Response response) {
                AdaptadorPreEstrenos adaptadorPreEstrenos = new AdaptadorPreEstrenos(MainActivity.this, PreEstr);
                estrenos.setAdapter(adaptadorPreEstrenos);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        estrenos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentTitular = new Intent(MainActivity.this, detalleEstreno.class);
                PreEstrenos peliculaEstreno = (PreEstrenos) parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("TituloEstreno", peliculaEstreno.getTitulo());
                extras.putString("SinopsisEstreno", peliculaEstreno.getSinopsis());
                extras.putString("FechaInicialEstreno", peliculaEstreno.getFecha_inicio());
                extras.putString("FechaFinalEstreno", peliculaEstreno.getFecha_final());
                extras.putString("ImagenEstreno", peliculaEstreno.getImage());
                intentTitular.putExtras(extras);
                startActivity(intentTitular);
            }
        });


        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        //Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 1:
                Intent mostrarMain = new Intent(MainActivity.this, MainActivity.class);
                startActivity(mostrarMain);
                break;
            case 2:
                fragment = new MovieFragment();
                title = "Peliculas";
                break;
            case 3:
                fragment = new FavoritosFragment();
                title = "Favoritos";
                break;
            case 4:
                fragment = new funcionFragment();
                title = "Funciones";
                break;
            default:
                break;
        }

        if (fragment != null) {
            ((RelativeLayout) findViewById(R.id.ContainerLayout)).removeAllViewsInLayout();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ContainerLayout, fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        getMenuInflater().inflate(R.menu.menu_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.) {
        //  Intent ajustes = new  Intent(MainActivity.this, SettingsActivity.class);
        //startActivity(ajustes);
        //}
        if (id == R.id.action_settings) {
            Intent intento = new Intent(this, Login.class);
            startActivity(intento);

            //Limpiar usuario

            SharedPreferences pref = MainActivity.this.getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();

            //LimpiarFavoritos

            ManejadorFavorito.getInstancia().limpiarFavorito();

            finish();
            return true;
        }

        if (id == R.id.menu_perfil) {
            Intent mostrarPerfil = new Intent(this, ver_perfil.class);
            startActivity(mostrarPerfil);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }


    class AdaptadorPreEstrenos extends ArrayAdapter<PreEstrenos> {

        private List<PreEstrenos> listaPreEstrenos;

        public AdaptadorPreEstrenos(Context context, List<PreEstrenos> preEstren) {
            super(context, R.layout.estrenos_list, preEstren);
            listaPreEstrenos = preEstren;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.estrenos_list, null);

            ImageLoader imageLoader = ImageLoader.getInstance();

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true)
                    .showImageForEmptyUri(R.drawable.ic_movie)
                    .showImageOnFail(R.drawable.ic_movie)
                    .showImageOnLoading(R.drawable.ic_movie).build();

            ImageView imageView = (ImageView)item.findViewById(R.id.imageEstreno1);

            imageLoader.displayImage(listaPreEstrenos.get(position).getImage(), imageView, options);

            TextView textTitle12 = (TextView) item.findViewById(R.id.tituloM1);
            textTitle12.setText(listaPreEstrenos.get(position).getTitulo());

            TextView textFecha12 = (TextView) item.findViewById(R.id.fechaM2);
            textFecha12.setText(listaPreEstrenos.get(position).getFecha_inicio());

            return item;
        }


    }
}

