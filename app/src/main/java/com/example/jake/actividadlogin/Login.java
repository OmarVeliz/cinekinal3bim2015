package com.example.jake.actividadlogin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.example.jake.actividadlogin.SqlLite.DbHelper;
import com.example.jake.actividadlogin.manejador.ManejadorUsuario;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;

public class Login extends ActionBarActivity {

    EditText editText;
    EditText editText2;
    private Toolbar mToolbar;
    private String name;
    private String correo;
    private CheckBox btnRecordarme;
    private Button butonLogin;
    private Button butonRegistro;
    private String correoUsuario = null;
    private String nameUsuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (!isOnline()){
            Toast.makeText(this.getApplicationContext(), "Please chech your network status", Toast.LENGTH_LONG).show();
        }

        //Parse.com
        // Enable Local Datastore.
        ParseAnalytics.trackAppOpenedInBackground(getIntent());

        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground();

        editText = (EditText)findViewById(R.id.editText);
        editText2 = (EditText)findViewById(R.id.editText2);
        butonLogin = (Button)findViewById(R.id.btnIniciarSesion);
        butonRegistro = (Button)findViewById(R.id.btnBienvenido);
        btnRecordarme = (CheckBox)findViewById(R.id.btnRecordarme);


        butonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Parse.com

                final String nombreUsuario = editText.getText().toString();
                String contrasenia = editText2.getText().toString();


                ParseUser.logInInBackground(nombreUsuario, contrasenia, new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {

                            correoUsuario = user.getEmail().toString();
                            nameUsuario = user.getUsername().toString();

                            SharedPreferences prefs =
                                    getSharedPreferences("MisPreferencias",Context.MODE_PRIVATE);

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("nombre", nombreUsuario);
                            editor.putString("correo", correoUsuario);
                            editor.putString("nombre", nameUsuario);
                            editor.commit();

                            Intent intento = new Intent(Login.this, MainActivity.class);
                            startActivity(intento);
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "Credenciales no validas",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }
        });

        butonRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent paginaRegistro = new Intent(Login.this, Register.class);
                startActivity(paginaRegistro);
                finish();
            }
        });
    }

    public boolean isOnline(){
        ConnectivityManager manager = (ConnectivityManager)this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = manager.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
           // return true;
        //}

        return super.onOptionsItemSelected(item);
    }

}
