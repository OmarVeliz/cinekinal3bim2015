package com.example.jake.actividadlogin.beans;

/**
 * Created by jake on 19/07/15.
 */
public class PreEstrenos {

    private String titulo;
    private String sinopsis;
    private String image;
    private String fecha_inicio;
    private String fecha_final;
    private String trailer_url;

    public String getTitulo() {
        return titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public String getImage() {
        return image;
    }

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public String getFecha_final() {
        return fecha_final;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
