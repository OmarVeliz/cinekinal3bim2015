package com.example.jake.actividadlogin;

import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

//Cargar imagenes
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class mostrarDetalles extends ActionBarActivity {

    private Toolbar mToolbar;
    private TextView nombrePeliculaDetalle, descripcionPeliculaDetalle, generoPelicula, clasificacionPelicula;
    private VideoView trailerPelicula;
    private ImageView imagen;
    private String imagenSeleccionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_detalles);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        nombrePeliculaDetalle = (TextView)findViewById(R.id.nombrePeliculaDetalleEstreno);
        descripcionPeliculaDetalle = (TextView)findViewById(R.id.descripcionEstreno);
        generoPelicula = (TextView)findViewById(R.id.fechaInicio);
        clasificacionPelicula = (TextView)findViewById(R.id.fechaFinal);
        trailerPelicula = (VideoView)findViewById(R.id.videoTrailer);
        imagen = (ImageView)findViewById(R.id.imageEstreno1);

        Bundle extras = getIntent().getExtras();

        nombrePeliculaDetalle.setText("Titulo: "+ extras.getString("Titulo"));
        descripcionPeliculaDetalle.setText("Descripcion: " + extras.getString("Descripcion"));
        generoPelicula.setText("Genero: " + extras.getString("Genero"));
        clasificacionPelicula.setText("Clasificacion: " + extras.getString("Clasificacion"));

        imagenSeleccionada = extras.getString("Imagen");
        //trailerPelicula.setVideoPath(extras.getString("Trailer"));

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        imagen = (ImageView)findViewById(R.id.imageEstreno1);

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.ic_movie)
                .showImageOnFail(R.drawable.ic_movie)
                .showImageOnLoading(R.drawable.ic_movie).build();

        imageLoader.displayImage(imagenSeleccionada, imagen, options);

        MediaController mc = new MediaController(this);

        trailerPelicula.setVideoURI(Uri.parse(extras.getString("Trailer")));
        trailerPelicula.setMediaController(mc);
        trailerPelicula.requestFocus();
        trailerPelicula.start();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mostrar_detalles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home){
            mostrarDetalles.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
