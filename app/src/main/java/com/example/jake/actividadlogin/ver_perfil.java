package com.example.jake.actividadlogin;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jake.actividadlogin.SqlLite.DbHelper;


public class ver_perfil extends ActionBarActivity {

    private Toolbar mToolbar;
    private EditText cambiarNombre;
    private EditText cambiarCorreo;

    private String usuarioAnterior;
    private String emailAnterior;
    private Button guardarCambios;

    //
    private String nuevoUsuario;
    private String nuevoEmail;

    private int numeroUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_perfil);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cambiarNombre = (EditText)findViewById(R.id.cambiarNombre);
        cambiarCorreo = (EditText)findViewById(R.id.cambiarEmail);

        guardarCambios = (Button)findViewById(R.id.guardarCambios);


        SharedPreferences pref = this.getApplicationContext().getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
        usuarioAnterior = pref.getString("NombreUsuario", null);
        emailAnterior = pref.getString("emailUsuario", null);
        numeroUsuario = pref.getInt("IdUsuario", 0);



        cambiarNombre.setHint(usuarioAnterior);
        cambiarCorreo.setHint(emailAnterior);


        //Guardar los cambios

        guardarCambios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nuevoUsuario = cambiarNombre.getText().toString();
                nuevoEmail = cambiarCorreo.getText().toString();

                //Toast.makeText(getApplicationContext(), "Datos aceptados" + numeroUsuario,
                  //  Toast.LENGTH_LONG).show();

                DbHelper cambiarUsuario = new DbHelper(ver_perfil.this, "DBCine", null, 1);
                SQLiteDatabase db = cambiarUsuario.getWritableDatabase();

                db.execSQL("UPDATE Usuarios SET name= '" + nuevoUsuario +"' WHERE id = '" + numeroUsuario +"' ");
                db.execSQL("UPDATE Usuarios SET email= '" + nuevoEmail +"' WHERE id = '" + numeroUsuario +"' ");

                SharedPreferences preferences = getSharedPreferences("DatosUsuario", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("NombreUsuario", nuevoUsuario);
                editor.putString("emailUsuario", nuevoEmail);

                Intent pestañaPrincipal = new Intent(ver_perfil.this, MainActivity.class);
                startActivity(pestañaPrincipal);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ver_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
