package com.example.jake.actividadlogin;

import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


public class detalleEstreno extends ActionBarActivity {


    private Toolbar mToolbar;
    private TextView nombrePeliculaDetalleEstreno, fechaInicio, fechaFinal, descripcionEstreno;
    private VideoView videoTrailerEstreno;
    private ImageView imagen;
    private String imagenSeleccionada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_estreno);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nombrePeliculaDetalleEstreno = (TextView)findViewById(R.id.nombrePeliculaDetalleEstreno);
        fechaInicio = (TextView)findViewById(R.id.fechaInicio);
        fechaFinal = (TextView)findViewById(R.id.fechaFinal);
        descripcionEstreno = (TextView)findViewById(R.id.descripcionEstreno);

        videoTrailerEstreno = (VideoView)findViewById(R.id.videoTrailer);
        imagen = (ImageView)findViewById(R.id.imageEstreno1);

        Bundle extras = getIntent().getExtras();

        nombrePeliculaDetalleEstreno.setText("Titulo: "+ extras.getString("TituloEstreno"));
        descripcionEstreno.setText("Descripcion: " + extras.getString("SinopsisEstreno"));
        fechaInicio.setText("Fecha inicio: " + extras.getString("FechaInicialEstreno"));
        fechaFinal.setText("Fecha final: " + extras.getString("FechaFinalEstreno"));

        //videoTrailerEstreno.setVideoPath(uriVideo);
        imagenSeleccionada = extras.getString("ImagenEstreno");

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);

        imagen = (ImageView)findViewById(R.id.imageEstreno1);

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.drawable.ic_movie)
                .showImageOnFail(R.drawable.ic_movie)
                .showImageOnLoading(R.drawable.ic_movie).build();

        imageLoader.displayImage(imagenSeleccionada, imagen, options);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detalle_estreno, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home){
            detalleEstreno.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
