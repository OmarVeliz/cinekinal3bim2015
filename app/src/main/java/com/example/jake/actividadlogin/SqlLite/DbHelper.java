package com.example.jake.actividadlogin.SqlLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Jake on 05/06/2015.
 */
public class DbHelper extends SQLiteOpenHelper {

    private String sqlCreate = "CREATE TABLE Usuarios (id INTEGER PRIMARY KEY, user TEXT, pass TEXT, name TEXT, email TEXT )";
    private String sqlMovies = "CREATE TABLE Movies (id INTEGER PRIMARY KEY, nameMovie TEXT, description TEXT )";
    private String sqlFavoritos = "CREATE TABLE Favoritos (id INTEGER PRIMARY KEY, titleMovie TEXT, generoMovie TEXT, nameUser TEXT)";

    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
        db.execSQL(sqlMovies);
        db.execSQL(sqlFavoritos);

        db.execSQL("INSERT INTO Movies VALUES (null,'Avengers infinity war', 'Los heroes mas poderos del mundo se enfrentan a thanos')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'Capitan america Civil war' , 'El enfrentamiento entre iron man y el capitan')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'Doc Strange' , 'El mago que tiene poderes misticos')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'Spiderman', 'Narra la vida de Peter Parker y como se convirtio en un heroe')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'La pantera negra', 'La pantera toma el lugar de su padre')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'El hombre hormiga' , 'Un cientifico que descubre como hacerse pequeño')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'Guardianes de la galaxia 2' , 'Los heroes se enfrentan a una nueva aventura')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'X-men origines', 'Narra la vida de los mutantes')");
        db.execSQL("INSERT INTO Movies VALUES (null,'Thor ragnarock' , 'Thor se enfrenta ante el apocalipsis de su mundo')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'Word hulk' , 'Despues de desaparecer hulk hace su regreso')");
        db.execSQL("INSERT INTO Movies VALUES (null, 'Los 4 fantasticos', 'De regreso a marvel')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Table users

        db.execSQL("DROP TABLE IF EXISTS Usuarios");
        db.execSQL(sqlCreate);

        //Table movies

        db.execSQL("DROP TABLE IF EXISTS Movies");
        db.execSQL(sqlMovies);

        //Table favorites

        db.execSQL("DROP TABLE IF EXISTS Favoritos");
        db.execSQL(sqlFavoritos);
    }
}