package com.example.jake.actividadlogin.manejador;

import com.example.jake.actividadlogin.beans.Favoritos;

import java.util.ArrayList;

/**
 * Created by Jake on 29/05/2015.
 */
public class ManejadorFavorito {
    private static ManejadorFavorito instancia;
    public ArrayList<Favoritos> agregarFavorito = new ArrayList<Favoritos>();

    public static ManejadorFavorito getInstancia(){
        if( instancia == null){
            instancia = new ManejadorFavorito();
        } return instancia;
    }

    public void nuevoFavorito (Favoritos favorito) {
        agregarFavorito.add(favorito);
    }

    public void limpiarFavorito () {
        agregarFavorito.clear();
    }

}
