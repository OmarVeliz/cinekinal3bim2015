package com.example.jake.actividadlogin.beans;

import java.util.Date;

/**
 * Created by jake on 19/07/15.
 */
public class Functions {

    private String titulo;
    private String nombre;
    private String fecha;
    private String hora;
    private String formato_lenguaje;
    private String image;

    public String getTitulo() {
        return titulo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getFormato_lenguaje() {
        return formato_lenguaje;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setFormato_lenguaje(String formato_lenguaje) {
        this.formato_lenguaje = formato_lenguaje;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Functions{" +
                "titulo='" + titulo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", fecha='" + fecha + '\'' +
                ", hora='" + hora + '\'' +
                ", formato_lenguaje='" + formato_lenguaje + '\'' +
                '}';
    }
}
