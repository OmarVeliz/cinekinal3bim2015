package com.example.jake.actividadlogin.beans;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by jake on 17/07/15.
 */
public class Movie {

    public int id;
    public String titulo;
    public String sinopsis;
    public String trailer_url;
    public String image;
    public String rated;
    public String genero;

    public int getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public String getImage() {
        return image;
    }

    public String getRated() {
        return rated;
    }

    public String getGenero() {
        return genero;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", sinopsis='" + sinopsis + '\'' +
                ", trailer_url='" + trailer_url + '\'' +
                ", image='" + image + '\'' +
                ", rated='" + rated + '\'' +
                ", genero='" + genero + '\'' +
                '}';
    }
}
