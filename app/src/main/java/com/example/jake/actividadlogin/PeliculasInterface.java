package com.example.jake.actividadlogin;

//import android.support.v7.util.SortedList;

import com.example.jake.actividadlogin.beans.Movie;

import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;


/**
 * Created by jake on 17/07/15.
 */
public interface PeliculasInterface {

    @GET("/peliculas")
    void getMovie(Callback<List<Movie>> callback);
}
