import android.app.Activity;
import android.os.Bundle;

import com.example.jake.actividadlogin.R;

/**
 * Created by jake on 20/07/15.
 */
public class Inicio extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
    }
}
